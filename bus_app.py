from flask import Flask, request, jsonify
import json, utils
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config["MONGO_DBNAME"] = "bus"
app.config["MONGO_URI"] = "mongodb://localhost:27017/bus"
mongo = PyMongo(app)


@app.route('/view_tickets', methods=['GET'])  # viewing all tickets with their status
def get_tickets():
    tickets_iter = mongo.db.ticketList.find({}, {'name': True, 'user': True, '_id': False})
    tickets = []
    for ticket in tickets_iter:
        tickets.append(ticket)
    return jsonify(tickets)


@app.route('/view_open_tickets', methods=['GET'])  # viewing all open tickets
def get_open_tickets():
    tickets_iter = mongo.db.ticketList.find({'user': {"$exists": False}}, {'name': True, '_id': False})
    tickets = []
    for ticket in tickets_iter:
        tickets.append(ticket)
    return jsonify(tickets)


@app.route('/view_closed_tickets', methods=['GET'])  # viewing all closed tickets
def get_close_tickets():
    tickets_iter = mongo.db.ticketList.find({'user': {"$exists": True}}, {'name': True, '_id': False})
    tickets = []
    for ticket in tickets_iter:
        tickets.append(ticket)
    return jsonify(tickets)


@app.route('/admin/add_seat/<int:seat_number>', methods=['POST'])  # adding new ticket(for inserting tickets)
def add_ticket(seat_number):
    if not 1 <= seat_number <= 40:
        return "Invalid seat Number entered"
    seat = {'name': seat_number}
    ticket_list = mongo.db.ticketList
    if ticket_list.find(seat).count() > 0:
        return "ticket already exists"
    else:
        ticket_list.insert(seat)
    return "ticket added"


@app.route('/view_ticket_status/<tc>', methods=['GET'])  # view status of specific ticket
def view_ticket_status(tc):
    if mongo.db.ticketList.find({'name': tc, 'user': {"$exists": False}}).count() > 0:
        return "Open Ticket"
    return "ticket closed"


@app.route('/cancel_ticket/<new_tc>', methods=['POST'])  # open the ticket
def cancel_ticket(new_tc):
    tickets = mongo.db.ticketList
    if not tickets.find({'name': new_tc}).count() > 0:
        return "Seat number doesn't exist"
    if not utils.is_booked(new_tc, tickets):
        return "Seat is not booked"
    tickets.update({'name': new_tc}, {'$unset': {'user': ''}})
    return "Ticket cancelled successfully"


@app.route('/book_ticket', methods=['POST'])  # close ticket with the very user
def book_ticket():  # if that ticket is not allotted
    if request.json is None:
        return "Please provide seat number and user details"
    req_data = json.loads(request.json)
    coll = mongo.db.ticketList

    if "seat_number" not in req_data:
        return "Seat number not provided"
    seat_num = str(req_data["seat_number"])
    if not utils.exists(seat_num, coll):
        return "Seat number " + seat_num + " doesn't exist"

    if "user" not in req_data:
        return "Please provide user details"
    if utils.is_booked(seat_num, coll):
        return "Seat already booked"
    coll.update({'name': seat_num},
                {'$set': {'user': req_data["user"]}})
    return "Ticket booked successfully"


@app.route('/view_user/<tc>', methods=['GET'])  # viewing details of user owning the ticket
def get_user(tc):
    tickets_iter = mongo.db.ticketList.find({'name': tc}, {'user': True, '_id': False})
    if tickets_iter.count() == 0:
        return "Seat number doesn't exist"
    user = []
    for ticket in tickets_iter:
        user.append(ticket)
    return jsonify(user)


@app.route('/admin/open_all', methods=['POST'])  # close ticket with the very user if it's not already allotted
def open_all():
    ticket_list = mongo.db.ticketList.update({}, {'$unset':  {'user': ''}}, multi=True)
    return "All tickets Opened"


if __name__ == "__main__":
    app.run(debug=True)
