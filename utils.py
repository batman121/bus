def is_booked(seat_number, coll):
    return coll.find({'name': seat_number, 'user': {"$exists": True}}).count() > 0


def exists(seat_number, coll):
    return coll.find({'name': seat_number}).count() > 0
