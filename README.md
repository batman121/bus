# README #

Connect to postman and create a running mongodb database(from terminal) as following
app.config["MONGO_DBNAME"] = "bus"
app.config["MONGO_URI"] = "mongodb://localhost:27017/bus"

Import DataDump.json into your local env.

mongoimport --db bus --collection ticketList --file fileName.json --jsonArray



Use Mongo DB compass community edition to keep track of database

Use the following APIs from postman


POST==>http://127.0.0.1:5000//admin/add_seat/7===> adding a new seat in database
(40 tickets don't look practical for testing, hence, added this API )


POST====>http://127.0.0.1:5000/book_ticket   enter in raw form (json) ==>("{\"seat_number\": 3, \"user\": \"u3\"}") 
==> Update ticket 3 with user u3 if ticket 3 is not already allotted

POST====>http://127.0.0.1:5000/cancel_ticket/3 ===> open ticket 3


GET==> http://127.0.0.1:5000/view_ticket_status/3===> Return status of ticket 3

GET ==> http://127.0.0.1:5000/view_tickets ===> view all tickets

GET ==> http://127.0.0.1:5000/view_closed_tickets====> All closed tickets

GET====> http://127.0.0.1:5000/view_open_tickets ===> All opened tickets

GET===>http://127.0.0.1:5000/view_user/4==> return user details if  ticket is closed else appropriate message

POST===> http://127.0.0.1:5000///admin/open_all ===> all tickets will be opened 

